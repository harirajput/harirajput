package com.maxdigi.kayascreens.Registration;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.maxdigi.kayascreens.R;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterYourself extends Activity implements View.OnClickListener {
    EditText et_firstName,et_lastName,et_mobile,et_email;
    String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    //Button submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_register_yourself);
        et_firstName=findViewById (R.id.et_firstName);
        et_lastName=findViewById (R.id.et_lastName);
        et_mobile=findViewById (R.id.et_mobile);
        et_email=findViewById (R.id.et_email);
        findViewById (R.id.btn_submit).setOnClickListener (this);

        //submit.setOnClickListener (this);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        String first_name = et_firstName.getText().toString().trim();
        String last_name = et_lastName.getText().toString().trim();
        String email = et_email.getText().toString().trim();
        String mobile = et_mobile.getText().toString().trim();

        if (TextUtils.isEmpty(first_name)) {
            et_firstName.setError("Enter First Name");
            et_firstName.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(last_name)) {
            et_lastName.setError("Enter Last Name");
            et_lastName.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(mobile)) {
            et_mobile.setError("Enter Mobile Number");
            et_mobile.requestFocus();
            return;
        }
        if (mobile.length() < 10) {
            et_mobile.setError("Mobile Number must be 10 digit number");
            et_mobile.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(email)) {
            et_email.setError("Enter Email");
            et_email.requestFocus();
            return;
        }
        if (!isValidEmail(email)) {
            et_email.setError("Enter Valid Email");
            et_email.requestFocus();
            return;
        }

      /*  Map<String, String> params = new HashMap<> ();
        params.put("xAction", "registerUser");
        params.put("userFName", first_name);
        params.put("userLName", last_name);
        params.put("userMobile", mobile);
        params.put("userEmail", email);*/
    }
    boolean isValidEmail(String email) {
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}

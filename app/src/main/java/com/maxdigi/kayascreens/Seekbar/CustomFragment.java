package com.maxdigi.kayascreens.Seekbar;

import android.view.View;
import android.widget.TextView;

import com.maxdigi.kayascreens.R;
import com.warkiz.widget.Builder;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

/**
 * created by zhuangguangquan on  2017/9/6
 */

public class CustomFragment extends BaseFragment {
    Builder builder;
    String ar[]={"Below 25","26-30","31-35","35+"};
    @Override
    protected int getLayoutId() {
        return R.layout.custom;
    }

    @Override
    protected void initView(View root) {
        IndicatorSeekBar listenerSeekBar = root.findViewById(R.id.listener);
        listenerSeekBar.customTickTexts (ar);
        listenerSeekBar.tickTextsColor (getResources ().getColor (R.color.color_black),getResources ().getColor (R.color.color_tick));
        listenerSeekBar.thumbColor (getResources ().getColor (R.color.color_tick));

        final TextView thumb_position = root.findViewById(R.id.thumb_position);
        thumb_position.setText("thumb_position: ");
        final TextView tick_text = root.findViewById(R.id.tick_text);
        tick_text.setText("tick_text: ");
        listenerSeekBar.setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {
                thumb_position.setText("thumb_position: " + seekParams.thumbPosition);
                tick_text.setText("tick_text: " + seekParams.tickText);
            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

                //seekBar.tickTextsColorStateList (getResources ().getColorStateList (R.color.selector_tick_texts_2_color_thumb));

            }
        });


    }

}

package com.maxdigi.kayascreens.Seekbar;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.maxdigi.kayascreens.ProductDisplay.ProductDisplay;
import com.maxdigi.kayascreens.ProductDisplay.ProductsActivity;
import com.maxdigi.kayascreens.R;
import com.maxdigi.kayascreens.Registration.RegisterYourself;

public class MainActivity extends AppCompatActivity {
    private CustomFragment mCustomFragment;
    private static String[] sType = new String[]{"custom"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_main);
        initViews ();
    }

    private void initViews() {
        ViewPager viewPager = findViewById (R.id.viewPager);
        TabLayout tabLayout = findViewById (R.id.tabLayout);

        viewPager.setAdapter (new PagerAdapter (getSupportFragmentManager ()));
        tabLayout.setupWithViewPager (viewPager);

        for (String s : sType) {
            TextView textView = new TextView (this);
            textView.setText (s);
            tabLayout.newTab ().setCustomView (textView);
        }

    }

    private class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super (fm);
        }

        @Override
        public Fragment getItem(int position) {

            if (mCustomFragment == null) {
                mCustomFragment = new CustomFragment ();
            }
            return mCustomFragment;

        }

        @Override
        public int getCount() {
            return sType.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return sType[position];
        }
    }


    public void startactRegister(View view){
        startActivity (new Intent (MainActivity.this, RegisterYourself.class));
    }
    public void startactProducts(View view){
        startActivity (new Intent (MainActivity.this, ProductsActivity.class));
    }
}

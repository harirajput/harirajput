package com.maxdigi.kayascreens.ProductDisplay;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.maxdigi.kayascreens.R;
import com.squareup.picasso.Picasso;

import java.util.List;


public class ProductRecyclerAdapter extends RecyclerView.Adapter<ProductRecyclerAdapter.ProductDisplayViewHolder> {
    private List<ProductDisplay> ProductDisplayList;
    private Context context;

    public ProductRecyclerAdapter(List<ProductDisplay> ProductDisplayList, Context context) {
        this.ProductDisplayList = ProductDisplayList;
        this.context = context;
    }

    void dis(){
        Toast.makeText (context,"",Toast.LENGTH_LONG).show ();
    }
    @NonNull
    @Override
    public ProductDisplayViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //inflate the layout file
        View ProductDisplayProductView = LayoutInflater.from (parent.getContext ()).inflate (R.layout.recycler_item, parent, false);
        return new ProductDisplayViewHolder (ProductDisplayProductView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductDisplayViewHolder holder, final int position) {
        //Picasso.get ().load ("http://mall.coimbatore.com/bnh/shampoo/clinic-plus/clinic-plus-soft-&-silky-cream-conditioner.jpg").into (holder.imageView);
        Glide.with(context).load(ProductDisplayList.get (position).getProductImage ()).into(holder.imageView);
        holder.txtview.setText (ProductDisplayList.get (position).getProductName ());
    }
    @Override
    public int getItemCount() {
        return ProductDisplayList.size ();
    }

    public class ProductDisplayViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView txtview;

        public ProductDisplayViewHolder(View view) {
            super (view);
            imageView = view.findViewById (R.id.idProductImage);
            txtview = view.findViewById (R.id.idProductName);
        }
    }
}
package com.maxdigi.kayascreens.ProductDisplay;
public class ProductDisplay {
    public String productImage;
    public String productName;

    public ProductDisplay(String productName, String productImage) {
        this.productImage = productImage;
        this.productName = productName;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
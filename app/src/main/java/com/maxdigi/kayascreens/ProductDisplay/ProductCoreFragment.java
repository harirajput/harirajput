package com.maxdigi.kayascreens.ProductDisplay;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.maxdigi.kayascreens.R;

import java.util.ArrayList;
import java.util.List;

public class ProductCoreFragment extends Fragment {
    private List<ProductDisplay> productDisplayList = new ArrayList<> ();
    RecyclerView recyclerView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate (R.layout.display_product_fragment, container, false);
        recyclerView = view.findViewById (R.id.recyclerViewProductList);
        productDisplayList.add (new ProductDisplay ("p1", "http://mall.coimbatore.com/bnh/shampoo/clinic-plus/clinic-plus-soft-&-silky-cream-conditioner.jpg"));
        productDisplayList.add (new ProductDisplay ("p2", "http://goo.gl/gEgYUd"));

        //recyclerView.setLayoutManager (new LinearLayoutManager (getActivity (), LinearLayoutManager.HORIZONTAL, false));
        //recyclerView.setItemAnimator (new DefaultItemAnimator ());

        LinearLayoutManager itemslayoutManager = new LinearLayoutManager(getActivity ());
        itemslayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(itemslayoutManager);

        recyclerView.setAdapter (new ProductRecyclerAdapter (productDisplayList, getActivity ()));
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated (view, savedInstanceState);


    }
}

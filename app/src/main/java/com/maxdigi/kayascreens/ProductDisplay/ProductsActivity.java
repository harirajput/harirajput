package com.maxdigi.kayascreens.ProductDisplay;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.maxdigi.kayascreens.R;

public class ProductsActivity extends AppCompatActivity implements View.OnClickListener {
    Button btn_skin_core,btn_skin_regime,btn_previous,btn_next;
    String tag = "MyFragment";
    int flag=0;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.display_products);
        btn_skin_core=findViewById (R.id.btn_skin_core);
        btn_skin_regime=findViewById (R.id.btn_skin_regime);
        btn_previous=findViewById (R.id.btn_previous);
        btn_next=findViewById (R.id.btn_next);

        btn_skin_core.setOnClickListener (this);
        btn_skin_regime.setOnClickListener (this);
        btn_previous.setOnClickListener (this);
        btn_next.setOnClickListener (this);
        btn_skin_core.setSelected (true);

        replaceFragment (this,new ProductCoreFragment (),true);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if (v==btn_next){
            if (flag==0) {
                flag = 1;
                replaceFragment (ProductsActivity.this,new ProductRegimeFragment(),true);
                btn_previous.setVisibility (View.VISIBLE);
                return;
            }

            //startActivity ();
        }else if (v==btn_previous){
            flag=0;
            replaceFragment (ProductsActivity.this,new ProductCoreFragment(),true);

        }
        else {
            Button button = (Button) v;

            btn_skin_core.setSelected (false);
            btn_skin_core.setPressed (false);
            btn_skin_core.setTextColor (getResources ().getColor (R.color.color_tick));
            btn_skin_regime.setSelected (false);
            btn_skin_regime.setPressed (false);
            btn_skin_regime.setTextColor (getResources ().getColor (R.color.color_tick));

            // change state
            button.setSelected (true);
            button.setPressed (false);
            button.setTextColor (getResources ().getColor (R.color.color_white));
        }
    }

    void replaceFragment(Activity activity, Fragment frag, boolean flagsAddToBackStack) {
        android.support.v4.app.FragmentManager fm = ((AppCompatActivity) activity).getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.productsFrameLayout, frag, tag);
        if (flagsAddToBackStack) {
            if (ft.isAddToBackStackAllowed()) {
                ft.addToBackStack(null);
                ft.commit();
            }
        }
    }
}
